//
//  HelpSegmentViewController.swift
//  Helper
//
//  Created by Роман Хоменко on 17.06.2022.
//

import UIKit

class HelpSegmentViewController: UIViewController {

    @IBOutlet weak var testLabel: UILabel!
    var displayText: String?
    
    var index: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        testLabel.text = displayText
    }

}

//
//  ViewController.swift
//  Helper
//
//  Created by Роман Хоменко on 17.06.2022.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var contentView: UIView!
    
    let dataSource = ["View controller test1", "View controller test2", "View controller test3"]
    
    var pageIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .blue
        configurePageViewController()
    }
}

// configure VCs
extension ViewController {
    func configurePageViewController() {
        guard let pageVC = storyboard?.instantiateViewController(withIdentifier: String(describing: PageViewController.self)) as? PageViewController else { return }
        
        pageVC.delegate = self
        pageVC.dataSource = self
        
        addChild(pageVC)
        pageVC.didMove(toParent: self)
        
        pageVC.view.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.addSubview(pageVC.view)
        
        let views: [String: UIView] = ["pageView": pageVC.view]
        configureConstraintsPageViewController(for: views)
        
        guard let startingVC = helpSegmentViewControllerAt(index: pageIndex) else { return }
        
        pageVC.setViewControllers([startingVC], direction: .forward, animated: true)
    }
    
    func helpSegmentViewControllerAt(index: Int) -> HelpSegmentViewController? {
        
        if index >= dataSource.count || dataSource.count == 0 {
            return nil
        }
        
        guard let helpSegmentVC = storyboard?.instantiateViewController(withIdentifier: String(describing: HelpSegmentViewController.self)) as? HelpSegmentViewController else { return nil }
        
        helpSegmentVC.index = index
        helpSegmentVC.displayText = dataSource[index]
        
        return helpSegmentVC
    }
    
    func configureConstraintsPageViewController(for views: [String: Any]) {
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[pageView]-0-|",
                                                                  options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                  metrics: nil,
                                                                  views: views))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[pageView]-0-|",
                                                                  options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                  metrics: nil,
                                                                  views: views))
    }
}

// dataSource implementation
extension ViewController: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return pageIndex
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return dataSource.count
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let helpSegmentVC = viewController as? HelpSegmentViewController
        
        guard var currentPage = helpSegmentVC?.index else { return nil }
        
        pageIndex = currentPage
        
        if currentPage == 0 {
            return nil
        }
        
        currentPage -= 1
        
        return helpSegmentViewControllerAt(index: currentPage)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let helpSegmentVC = viewController as? HelpSegmentViewController
        
        guard var currentPage = helpSegmentVC?.index else { return nil }
        
        if pageIndex == dataSource.count {
            return nil
        }
        
        currentPage += 1
        
        pageIndex = currentPage
        
        return helpSegmentViewControllerAt(index: currentPage)
    }
}
